import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  decrement,
  decrementByAmount,
  increment,
  incrementByAmount,
} from "./counterSlice";

export function Counter() {
  const count = useSelector((state) => state.counter.value);
  const dispatch = useDispatch();

  return (
    <div>
      <div>
        
        <button onClick={() => dispatch(decrementByAmount(20))}>
          decrementBy20
        </button>
        <button onClick={() => dispatch(decrement())}>Decrement</button>
        <span>{count}</span>
        <button onClick={() => dispatch(increment())}>Increment</button>
        <button onClick={() => dispatch(incrementByAmount(20))}>
          incrementBy20
        </button>
      </div>
    </div>
  );
}
